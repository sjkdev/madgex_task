# Todo/ workflow


> ### create main blocks for each section and corresponding njk templates
> ###### ~~Navigation section (already provided)~~
> ###### ~~Atf/ Home section~~
> ###### ~~Jobs/ featured employers section~~
> ###### ~~3 CTAs section (3 ctas one upload section)~~
> ###### ~~featured employers logo section~~
> ###### ~~careers advice section~~
> ###### ~~team treehouse/ advertise with us section~~
> ###### ~~About us/ Download app/ featured campaigns section~~
> ###### ~~footer section~~

> ###### njk templates named as per corresponding section of page (use better naming convetion?)

> ## create routing for navbar and browse jobs by sector section (btf) and featured jobs section (add to data model)
> ####### added extra nav menu items to navbar/ server/data/model.js
> ## create sign up/ sign in form

> ## create input fields for job search (use job api indeed? for dummy data) 
> #### - keyword/ location/ distance



> ## create 3 CTA's
> #### - instant job alerts
> #### - create account
> #### - upload resume



 > ## featured employers
 > #### - render as carousel
 > #### - ~~get company logo~~ and corresponding backlink for said co.
 > #### - create page route listing all employers

 > ## team treehouse ad
 > #### - get backlink to link to image

 > ## careers advice/ blog section
 > #### - ~~get images~~ and create route for posts

> ## advertise with us 
> #### - used image from pexels similar-ish to mockup (couldn't find exact image)

> ## penultimate section (pre-footer)
 > #### - about us
 > #### - download new link
 > #### - featured campaigns

 > ## footer
 > #### - create route/ navigation
 > #### - social media (fa icons?) and copyright



 Notes - ~~swapped out bulma for reflex-grid css framework as it is lighter  and is similar to naking convention in _grid.scss file~~ In the end decided to go with flexbox using a third party css framework seemed a bit overkill for a quick protoype