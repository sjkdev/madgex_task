const pkg = require('../../package.json');

module.exports = {
  appVersion: pkg.version,
  assetPath: '/public',
  pageTitle: 'PxlPro Jobs',
  navItems: [
    {
      label: 'Home',
      href: '#'
     
    },
    {
      label: 'Find a job',
      href: '#find-a-job'
 
    },
    {
      label: 'Careers advice',
      href: '#careers'
  
    },
    {
      label: 'Job alerts',
      href: '../../src/templates/partials/pages/job-alerts.njk'
 
    },
    {
      label: 'Your jobs',
      href: '#your-jobs'
   
    },
    {
      label: 'Advertise a job',
      href: '#advertise-a-job'

    }

  ],
  heroMessage: 'Search 1,570 jobs.',
  heroSubheading: 'Hundreds of new career opportunities each day. Find your perfect job.',
  inputKeyword: 'Keyword (e.g. Manager)',
  inputLocation: 'Location (e.g. Boston)',
  searchButton: 'Search',
  browseJobs: 'Browse jobs by sector',

  // these two job category columns should have the number of jobs 'catNo' populated dynamically by pulling in the data through a jobs api
  jobCatColOne: [
    {
      cat: 'Accountancy & Finanace',
      catNo: '41'
    },
    {
      cat: 'Architecture, Building and Planning',
      catNo: '71'},
    {
      cat: 'Art & Design',
      catNo: '59'},
    {
      cat: 'Building & Construction',
      catNo: '49'},
    {
      cat: 'Business Management',
      catNo: '247'},
    {
      cat: 'Catering & Hospitality',
      catNo: '14'},
    {
      cat: 'Computers & IT',
      catNo: '41'
    }
  ],
  jobCatColTwo: [
      {
        cat: 'Education',
        catNo: '193'
      },
      {
        cat: 'Engineering',
        catNo: '47'},
      {
        cat: 'Media & Journalism',
        catNo: '59'
      },
      {
        cat: 'Retail & Wholesale',
        catNo: '100'
      },
      {
        cat: 'Sales',
        catNo: '29'
      },      
      {
        cat: 'Sciences',
        catNo: '79'},
      {
        cat: 'Sports & Fitness',
        catNo: '72'}
  ],

  featuredJobs:[
    {
      img: '/public/images/_jobs/amex.png',
      job: 'amex',
      salary: '£200000'
    },
    {
      img: '../../public/images/_jobs/microsoft.png',
      job: '`Nasa',
      salary: '£4500000'
    },
    {
      img: '../../public/images/_jobs/nasa.png',
      job: 'Microsoft',
      salary: '£5000000'
    },
  ],

  ctas: [
    {
      icon: 'job-alerts',
      title: 'Get instant job alerts',
      subtitle: 'Personalised job recommendations sent straight to your email.'
    },
    {
      icon: 'create-account',
      title: 'Create an account for free',
      subtitle:
        'Shortlist jobs, manage your job alerts and receive special offers.'
    },
    {
      icon: 'upload-resume',
      title: 'Upload your resume',
      subtitle:
        'Upload your resume so our employers can match your details to the best jobs.'
    }
  ],
  footerItems: [
    {
      label: 'Browse jobs',
      href: '#browse-jobs'
    },
    {
      label: 'Search employees',
      href: '#search-employees'
    },
    {
      label: 'Terms & Conditions',
      href: '#terms-and-conditions'
    },
    {
      label: 'Privacy',
      href: '#privacy'
    },
    {
      label: 'About us',
      href: '#about-us'
    },
    {
      label: 'Contact us',
      href: '#contact-us'
    },
  ],

  powered: 'Powered by Madgex',

  socialMedia: [
    {
      label: 'Facebook',
      href:'#facebook',
      img:'../../public/images/_social/Facebook.svg'
    },
    {
      label: 'Twitter',
      href:'#twitter',
      img:'../../public/images/_social/Twitter.svg'
    },
    {
      label: 'Google+',
      href:'#google-plus',
      img:'../../public/images/_social/GooglePlus.svg'
    },
    {
      label: 'LinkedIn',
      href:'#linkedin',
      img:'../../public/images/_social/LinkedIn.svg'
    }
  ]
};
